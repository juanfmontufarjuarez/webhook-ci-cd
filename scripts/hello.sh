#!/usr/bin/bash
cd /root/nginx
git pull
docker build -t swr.na-mexico-1.myhuaweicloud.com/jfmontufar/nginx:webhook .
docker push swr.na-mexico-1.myhuaweicloud.com/jfmontufar/nginx:webhook
kubectl set image deploy/nginx container-0=swr.na-mexico-1.myhuaweicloud.com/jfmontufar/nginx:webhook
kubectl rollout restart deploy/nginx
